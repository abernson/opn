Package.describe({
  name: 'telescope-plugin-rich-text-editor',
  summary: 'A simple plugin that replaces the standard post body editor with rich text editor for your Telescope site.',
  version: '0.0.1',
  git: 'https://github.com/almogdesign/telescope-plugin-rich-text-editor.git'
});

Package.onUse(function(api) {

  api.versionsFrom("METEOR@1.0.4");

  api.use([
    'telescope:core@0.20.6',
    'autoform-froala@0.0.3'
  ]);

  api.addFiles([
    'lib/lib.js'
  ], ['client', 'server']);

});
