/* Remove default post body */
Posts.removeField("body");

/* Add rich text editor version */
Posts.addField({
    fieldName: 'body',
    fieldSchema: {
        type: String,
        optional: true,
        //max: 8000,
        editableBy: ["member", "admin"],
        autoform: {
            row:6,
            afFieldInput: {
                type: 'froala',
                inlineMode: false,
                buttons: ['bold', 'italic' , 'underline', 'createLink' , 'insertImage', 'formatBlock'] // 'insertVideo'
            }
        }
    }
});

