Posts.addField({
    fieldName: 'flagged',
    public: false,
    fieldSchema: {
        type: Boolean,
        optional: true,
        defaultValue: false,
        editableBy: ["member" , "admin"],
        autoform: {
            omit: true
        }
    }
});
