Meteor.methods({
    makeMember: function (userId) {
        Meteor.users.update({_id:userId}, {$set:{isGuest:false}});
    },
    makeGuest: function (userId) {
        Meteor.users.update({_id:userId}, {$set:{isGuest:true}});
    }
});